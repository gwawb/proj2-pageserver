from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:url>")
def trivia(url):
	if url[0] == "~" :
		return render_template("403.html")
	prev = " "
	for c in url:
		if prev == "/" and c == "/":
			return render_template("403.html")
		if prev == "." and c == ".":
			return render_template("403.html")
		prev = c
	try:
		return render_template(url)
	except:
		return render_template("404.html")
	#return render_template(url)
"""
@app.errorhandler(404)
def error_404(404):
	return render_template("404.html")
"""
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
